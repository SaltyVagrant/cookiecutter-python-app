===================================
Python Application Project Template
===================================

.. _cookiecutter template: https://gitlab.com/SaltyVagrant/cookiecutter-python-app
.. _Cookiecutter: http://cookiecutter.readthedocs.org
.. _Python Packaging User Guide: https://packaging.python.org/en/latest/distributing.html#configuring-your-project
.. _Packaging a Python library: http://blog.ionelmc.ro/2014/05/25/python-packaging

This template is a modification based on Michael Klatt's excellent `cookiecutter template`_.

This is a `Cookiecutter`_ template for creating a Python application project.

The project layout is based on the `Python Packaging User Guide`_. The current
conventional wisdom forgoes the use of a source directory, but moving the 
package out of the project root provides several advantages (*cf.* 
`Packaging a Python library`_).

Changes to original
-------------------

.. _tox: https://tox.readthedocs.io/en/latest/
.. _Docker: https://docker.com

- This template support Python3 only.
- `tox`_ added
- `Dockerfile`__ is provided to setup a build/test environment

__ Docker_

Template Project Features
=========================

.. _pytest: http://pytest.org
.. _Sphinx: http://sphinx-doc.org
.. _MIT License: http://choosealicense.com/licenses/mit

- `Docker`_
- Python 3.4+
- `MIT License`_
- `pytest`_ test suite
- `Sphinx`_ documentation
- `tox`_ documentation


Template Application Features
=============================

.. _YAML: http://pyyaml.org/wiki/PyYAML

- CLI with subcommands
- Logging
- Hierarchical `YAML`_ configuration


Usage
=====

.. _GitHub: https://gitlab.com/SaltyVagrant/cookiecutter-python-app


Install Python requirements for using the template:

.. code-block:: console

    $ python -m pip install --requirement=requirements.txt --user 


Create a new project directly from the template on `GitHub`_:

.. code-block:: console
   
    $ cookiecutter gh:mdklatt/cookiecutter-python-app

Docker use
==========

To use the Dockerfile, build the build/test image

.. code-block:: console

    $ docker build -t myimage:latest .

Run (default is to run `tox`_ tests):

.. code-block:: console

    $ docker run --rm -v "$(pwd)":/app myimage:latest


